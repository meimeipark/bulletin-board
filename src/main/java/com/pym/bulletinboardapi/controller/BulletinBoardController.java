package com.pym.bulletinboardapi.controller;


import com.pym.bulletinboardapi.model.*;
import com.pym.bulletinboardapi.service.BulletinBoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BulletinBoardController {
    private final BulletinBoardService bulletinBoardService;

    @PostMapping("/new")
    public String setBulletinBoard(@RequestBody BulletinBoardRequest request){
        bulletinBoardService.setBulletinBoard(request);

        return "글작성 완료";
    }

    @GetMapping("/list")
    public List<BulletinBoardItem> getBulletinBoards(){
        return bulletinBoardService.getBulletinBoards();
    }

    @GetMapping("/detail/{id}")
    public BulletinBoardResponse getBulletinBoard(@PathVariable long id){
        return bulletinBoardService.getBulletinBoard(id);
    }

    @PutMapping("/type/{id}")
    public String putTypeChange(@PathVariable long id, @RequestBody BulletinBoardTypeChangeRequest request){
        bulletinBoardService.putTypeChange(id, request);

        return "게시판 수정";
    }

    @PutMapping("/contents/{id}")
    public String putContentChange(@PathVariable long id, @RequestBody BulletinBoardContentChangeRequest request){
        bulletinBoardService.putContentChange(id, request);

        return "글 수정";
    }

    @DeleteMapping("/{id}")
    public String delBoard(@PathVariable long id){
        bulletinBoardService.delBoard(id);

        return "글 삭제";
    }
}
