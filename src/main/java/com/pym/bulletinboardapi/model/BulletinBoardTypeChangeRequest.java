package com.pym.bulletinboardapi.model;

import com.pym.bulletinboardapi.enums.BoardType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BulletinBoardTypeChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private BoardType boardType;
}
