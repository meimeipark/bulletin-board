package com.pym.bulletinboardapi.model;

import com.pym.bulletinboardapi.enums.BoardType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class BulletinBoardRequest {
    @Enumerated(value = EnumType.STRING)
    private BoardType boardType;
    private Long NO;
    private String textTitle;
    private String contents;
    private LocalDateTime creatDate;
    private String writer;
    private Integer clickCount;
}
