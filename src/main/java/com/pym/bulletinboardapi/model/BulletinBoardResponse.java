package com.pym.bulletinboardapi.model;

import com.pym.bulletinboardapi.enums.BoardType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class BulletinBoardResponse {
    private Long id;
    private String boardTypeName;
    private Long NO;
    private String textTitle;
    private String contents;
    private LocalDateTime createDate;
    private String writer;
    private Integer clickCount;
}
