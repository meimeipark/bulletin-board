package com.pym.bulletinboardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BulletinBoardContentChangeRequest {
    private String textTitle;
    private String contents;
}
