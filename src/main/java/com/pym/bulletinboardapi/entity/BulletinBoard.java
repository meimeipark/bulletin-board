package com.pym.bulletinboardapi.entity;

import com.pym.bulletinboardapi.enums.BoardType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.cglib.core.Local;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
public class BulletinBoard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private BoardType boardType;

    @Column(nullable = false)
    private Long NO;

    @Column(nullable = false, length = 150)
    private String textTitle;

    @Column(columnDefinition = "TEXT")
    private String contents;

    @Column(nullable = false)
    private LocalDateTime createDate;

    @Column(nullable = false, length = 30)
    private String writer;

    @Column(nullable = false)
    private Integer clickCount;
}
