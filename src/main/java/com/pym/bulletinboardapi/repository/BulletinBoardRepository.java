package com.pym.bulletinboardapi.repository;

import com.pym.bulletinboardapi.entity.BulletinBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BulletinBoardRepository extends JpaRepository <BulletinBoard, Long> {
}
