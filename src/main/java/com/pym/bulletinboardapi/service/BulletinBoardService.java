package com.pym.bulletinboardapi.service;



import com.pym.bulletinboardapi.entity.BulletinBoard;
import com.pym.bulletinboardapi.model.*;
import com.pym.bulletinboardapi.repository.BulletinBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class BulletinBoardService {
    private final BulletinBoardRepository bulletinBoardRepository;

    public void setBulletinBoard (BulletinBoardRequest request){
        BulletinBoard addData = new BulletinBoard();
        addData.setBoardType(request.getBoardType());
        addData.setNO(request.getNO());
        addData.setTextTitle(request.getTextTitle());
        addData.setContents(request.getContents());
        addData.setCreateDate(LocalDateTime.now());
        addData.setWriter(request.getWriter());
        addData.setClickCount(request.getClickCount());

        bulletinBoardRepository.save(addData);
    }

    public List <BulletinBoardItem> getBulletinBoards(){
        List<BulletinBoard> originList = bulletinBoardRepository.findAll();

        List<BulletinBoardItem> result = new LinkedList<>();

        for (BulletinBoard bulletinBoard: originList){
            BulletinBoardItem addItem = new BulletinBoardItem();
            addItem.setId(bulletinBoard.getId());
            addItem.setBoardTypeName(bulletinBoard.getBoardType().getName());
            addItem.setNO(bulletinBoard.getNO());
            addItem.setTextTitle(bulletinBoard.getTextTitle());
            addItem.setCreateDate(bulletinBoard.getCreateDate());
            addItem.setWriter(bulletinBoard.getWriter());

            result.add(addItem);
        }
        return result;
    }

    public BulletinBoardResponse getBulletinBoard(long id){
        BulletinBoard originData = bulletinBoardRepository.findById(id).orElseThrow();

        BulletinBoardResponse response = new BulletinBoardResponse();
        response.setId(originData.getId());
        response.setBoardTypeName(originData.getBoardType().getName());
        response.setNO(originData.getNO());
        response.setTextTitle(originData.getTextTitle());
        response.setContents(originData.getContents());
        response.setCreateDate(originData.getCreateDate());
        response.setWriter(originData.getWriter());
        response.setClickCount(originData.getClickCount());

        return response;
    }

    public void putTypeChange(long id, BulletinBoardTypeChangeRequest request){
        BulletinBoard originData = bulletinBoardRepository.findById(id).orElseThrow();
        originData.setBoardType(request.getBoardType());

        bulletinBoardRepository.save(originData);
    }

    public void putContentChange(long id, BulletinBoardContentChangeRequest request){
        BulletinBoard originData = bulletinBoardRepository.findById(id).orElseThrow();
        originData.setTextTitle(request.getTextTitle());
        originData.setContents(request.getContents());

        bulletinBoardRepository.save(originData);
    }

    public void delBoard(long id){bulletinBoardRepository.deleteById(id);}
}
