package com.pym.bulletinboardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BoardType {
    WEATHER("오늘의 날씨"),
    TODAY_ME("오늘의 나"),
    TRAVEL("떠나자");

    private final String name;
}
