package com.pym.bulletinboardapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "bulletinBoard App",
                description = "bulletinBoard app api명세",
                version = "v1")
)
@RequiredArgsConstructor
@Configuration
public class BulletinBoardConfig {

    @Bean
    public GroupedOpenApi bulletinBoardApi(){
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group(" 게시판 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
